require('marko/express'); //enable res.marko
require('marko/node-require').install();

var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var sqlite = require('sqlite3');
var knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: "./db/devdb.sqlite"
  },
  useNullAsDefault: true
});

var crypto = require('crypto');
var nodemailer = require('nodemailer');
var secrets = require('./secrets.json');

// Create the transporter with the required configuration for Gmail
// change the user and pass !
var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: secrets.gmailUsername,
        pass: secrets.gmailPassword
    }
});

// setup e-mail data
var mailOptions = {
    from: '"Jackson Stakeman" <jackson.stakeman@ruralsourcing.com>', // sender address (who sends)
    to: 'jstakeman@gmail.com', // list of receivers (who receives)
    subject: 'New Hire Ready for Assignment', // Subject line
    text: 'This new hire is ready ', // plaintext body
    html: '<b>Hello world </b><br> Some Link' // html body
};

// send mail with defined transport object

var app = express();

// Middleware

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('X-HTTP-Method'));
app.use(methodOverride('X-HTTP-Method-Override'));

//Helper Functions
function findCDM(center){
  if (center =='Albuquerque') {
    return 'paige.briggs@ruralsourcing.com';
  }
  else if (center =='Mobile') {
    return 'ingrid.miller@ruralsourcing.com';
  }
  else {
    return 'jstakeman@unm.edu';
  }
}

var dashboardView = require('./views/dashboard.marko')
app.get('/dash', function (req, res){
  knex.select().table('newhires').then(function (rows){
    res.marko(dashboardView, {
      newhires: rows
    })
  }
)
});

app.post('/login', function (req, res){
  var user = req.body.username.toLowerCase();
  if (user == 'serah.tyler') {
    //res.redirect('/dashboard');
    res.redirect('/dash')
  }
  else {
    //res.redirect('/login', {login: false})
    res.redirect('/login');
  }
});

var newhireView = require('./views/newhire.marko');
app.get('/newhire', function(req, res){
  res.marko(newhireView, {})
});

var dcdnewhireView = require('./views/newhire.marko');
app.get('/:hash', function (req, res){
  var hash = req.params.hash;
  knex('newhires').where({hash: hash}).then(function(row){
    var newhire = row[0];
    return res.marko(dcdnewhireView, row[0] )
  })
});

app.post('/newhire', function (req,res){
  var newEntry = {};
  newEntry.first_name = req.body.first_name;
  newEntry.last_name = req.body.last_name;
  var md5 = crypto.createHash('md5');
  var newHash = md5.update(newEntry.first_name + newEntry.last_name).digest('hex');
  newEntry.created_at = Date.now();
  newEntry.hash = newHash;
  newEntry.preffered_name = req.body.preffered_name;
  newEntry.rsi_email = req.body.rsi_email;
  newEntry.personal_email = req.body.personal_email;
  newEntry.dev_center = req.body.dev_center;
  newEntry.title = req.body.title;
  newEntry.date_initiated = req.body.date_initiated;
  newEntry.date_of_hire = req.body.date_of_hire;
  newEntry.pratice_area = req.body.pratice_area;
  knex('newhires').insert(newEntry).then(function (response) {
    console.log(response + 'Entry Added');

    var addressee = findCDM(newEntry.dev_center);
    var emailBody = `
    <p>Hello ${newEntry.dev_center} director,</p> \n
    <p>Your new hire ${newEntry.first_name} ${newEntry.last_name} needs to have
    a manager, mentor, and seat assigned.</p> \n
    <p>Please you the following link to continue the new hire process.</p> \n
    <a href="http://localhost:3000/${newEntry.hash}">Link to Onboarding App</a>
    `;

    mailOptions.to = addressee;
    mailOptions.html = emailBody;
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }

        console.log('Message sent: ' + info.response);
    });
    return res.redirect('/dashboard')

  }).catch(function(e){
    console.log(e);
  });

});

var dashboardView = require('./views/dashboard.marko')
app.get('/dash', function (req, res){
  knex.select().table('newhires').then(function (rows){
    res.marko(dashboardView, {
      newhires: rows
    })
  }
)
})

// API Routes
app.get('/api/v1/newhires', function(req, res){
  knex.select().table('newhires').then(function (rows) {
    return res.json(rows);
  })
});

app.post('/api/v1/newhires', function(req,res){
  var newEntry = {};
  newEntry.first_name = req.body.first_name;
  newEntry.last_name = req.body.last_name;
  var md5 = crypto.createHash('md5');
  var newHash = md5.update(newEntry.first_name + newEntry.last_name).digest('hex');
  newEntry.created_at = Date.now();
  newEntry.hash = newHash;
  newEntry.preffered_name = req.body.preffered_name;
  newEntry.rsi_email = req.body.rsi_email;
  newEntry.personal_email = req.body.personal_email;
  newEntry.dev_center = req.body.dev_center;
  newEntry.title = req.body.title;
  newEntry.date_initiated = req.body.date_initiated;
  newEntry.date_of_hire = req.body.date_of_hire;
  newEntry.pratice_area = req.body.pratice_area;
  knex('newhires').insert(newEntry).then(function (response) {
    console.log(response + 'Entry Added');

    var addressee = findCDM(newEntry.dev_center);
    var emailBody = `
    <p>Hello ${newEntry.dev_center} director,</p> \n
    <p>Your new hire ${newEntry.first_name} ${newEntry.last_name} needs to have
    a manager, mentor, and seat assigned.</p> \n
    <p>Please you the following link to continue the new hire process.</p> \n
    <a href="http://localhost/newhire/${newEntry.hash}">Link to Onboarding App</a>
    `;

    mailOptions.to = addressee;
    mailOptions.html = emailBody;
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }

        console.log('Message sent: ' + info.response);
    });
    return res.json({success: true});

  }).catch(function(e){
    console.log(e);
  });

});






// API
app.get('/api/v1/newhires/:hash', function (req,res){
  var hash = req.params.hash;
  knex('newhires').where({hash: hash}).then(function(row){
    return res.json(row[0]);
  })
});

app.put('/api/v1/newhires/:hash', function (req, res){
  var updateHash = req.params.hash;
  knex('newhires').where('hash', updateHash)
  .update(req.body).then(function(){
    return res.json({success: true})
  }).catch(function(e){
    console.log(e);
  })
})

app.delete('/api/v1/newhires/:hash', function (req, res) {
  var deleteHash = req.params.hash;
  knex('newhires').where('hash', deleteHash).del()
  .then( function () {
        return res.json({success: true});
  })
  .catch( function(e) {
    console.log(e);
  })
});

app.get('*', function (req, res){
  res.sendFile(__dirname + '/public/index.html');
})

app.listen(3000, function () {
  console.log('Listening on port 3000!')
})
