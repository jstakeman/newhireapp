angular.module('newhire', ['ngRoute'])
  .config(['$locationProvider', '$routeProvider',
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.
      when('/', {
        template: '<login></login>'
      }).
      when('/dashboard', {
        template: '<dashboard></dashboard>'
      })
  }
]);
