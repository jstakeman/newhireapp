angular.
  module('newhire').
  component('login', {
    template: `
    <hgroup>
    <!--<h1>RSI Login</h1>-->
    <a href="https://www.ruralsourcing.com/">
      <img src="images/rsi.png">
    </a>
  </hgroup>
  <form>
    <div class="group">
      <input ng-model="$ctrl.username" name="username" type="text"><span class="highlight"></span><span class="bar"></span>
      <label>User Name</label>
    </div>
    <div class="group">
      <input ng-model="$ctrl.password" name="password" type="password"><span class="highlight"></span><span class="bar"></span>
      <label>Password</label>
    </div>
    <button type="button" class="button buttonBlue">Login
      <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
    </button>
    <span class="group"><a href="#!/dashboard">Forgot password?</a></span>
  </form>
    `,
    controller: function LoginController() {
      this.username = '';
      this.password = '';
    }
  })
