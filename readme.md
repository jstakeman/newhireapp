#New Hire Project

##Start by running npm install

Then add a secrets.json file with objects gmailUsername and gmailPassword with your work email and password

**Run the following command to initialize the database**

``` npm run initidb

**The following command will run the server in development mode and restart on any changes**
```npm run dev
