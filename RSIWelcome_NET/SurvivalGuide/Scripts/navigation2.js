﻿// navigation2.js by Israel Montoya
// loads navigation into page from one level down.
document.write(`
<nav id="nav">
    <ul>
        <li><a href="../index.html">Home</a></li>

        <li>
            <a>What is RSI</a>
            <ul>
                <li><a href="../about.html">About RSI</a></li>
                <li><a href="whatisrsi.html">What Is RSI</a></li>
                <li><a href="corevalue.html">Core Values</a></li>
                <li><a href="source.html">The Source</a></li>
                <li><a href="rsiculture.html">RSI Culture</a></li>
            </ul>
        </li>
        <li>
            <a>Who is RSI</a>
            <ul>
                <li><a href="leaders.html">RSI Leaders</a></li>
                <li><a href="../contact.html">Colleagues</a></li>
                <li><a href="itsupport.html">IT Support</a></li>
            </ul>
        </li>
        <li>
            <a>Where is RSI</a>
            <ul>
                <li><a href="centers.html">RSI Development Centers</a></li>
                <li><a href="../map.html">Office Map</a></li>
            </ul>
        </li>
        <li>
            <a>What to know</a>
            <ul>
                <li><a href="itsecurity.html">IT</a></li>
                <li><a href="training.html">Training</a></li>
                <li><a href="bench.html">The Bench</a></li>
            </ul>
        </li>
        <li>
            <a>RSI Terms</a>
            <ul>
                <li><a href="glossary.html">Glossary</a></li>
            </ul>
        </li>
    </ul>
</nav>
`);