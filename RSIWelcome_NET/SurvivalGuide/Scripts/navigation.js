﻿// navigation.js by Israel Montoya
// loads navigation into page.
document.write(`
<nav id="nav">
    <ul>
        <li><a href="index.html">Home</a></li>

        <li>
            <a>What is RSI</a>
            <ul>
                <li><a href="about.html">About RSI</a></li>
                <li><a href="survivalguide/whatisrsi.html">What Is RSI</a></li>
                <li><a href="survivalguide/corevalue.html">Core Values</a></li>
                <li><a href="survivalguide/source.html">The Source</a></li>
                <li><a href="survivalguide/rsiculture.html">RSI Culture</a></li>
            </ul>
        </li>
        <li>
            <a>Who is RSI</a>
            <ul>
                <li><a href="survivalguide/leaders.html">RSI Leaders</a></li>
                <li><a href="contact.html">Colleagues</a></li>
                <li><a href="survivalguide/itsupport.html">IT Support</a></li>
            </ul>
        </li>
        <li>
            <a>Where is RSI</a>
            <ul>
                <li><a href="survivalguide/centers.html">RSI Development Centers</a></li>
                <li><a href="map.html">Office Map</a></li>
            </ul>
        </li>
        <li>
            <a>What to know</a>
            <ul>
                <li><a href="survivalguide/itsecurity.html">IT</a></li>
                <li><a href="survivalguide/training.html">Training</a></li>
                <li><a href="survivalguide/bench.html">The Bench</a></li>
            </ul>
        </li>
        <li>
            <a>RSI Terms</a>
            <ul>
                <li><a href="survivalguide/glossary.html">Glossary</a></li>
            </ul>
        </li>
    </ul>
</nav>
`);