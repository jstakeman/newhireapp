﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurvivalGuide.Models
{
    public class About
    {
        public List<Person> People { get; set; }
    }
}