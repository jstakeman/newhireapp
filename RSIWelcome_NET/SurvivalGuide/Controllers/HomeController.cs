﻿using SurvivalGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurvivalGuide.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult About()
        {
            Person person = new Person()
            {
                FirstName = "Eduardo",
                LastName = "Vazquez"

            };

            Person person2 = new Models.Person()
            {
                FirstName = "John",
                LastName = "Tom"
            };

            About about = new Models.About();
            about.People = new List<Person>();
            about.People.Add(person);
            about.People.Add(person2);



            return View(about);
        }

        public ActionResult Contact()
        {
            return View();
        }
       

            
        

        public ActionResult Activities()
        {
            return View();
        }

        public ActionResult Activities_ags()
        {
            return View();
        }
        public ActionResult Activities_jbr()
        {
            return View();
        }
        public ActionResult Activities_mod()
        {
            return View();
        }
        public ActionResult Congratulations()
        {
            return View();
        }

        public ActionResult Abq()
        {
            return View();
        }

        public ActionResult Ags()
        {
            return View();
        }
        public ActionResult Bench()
        {
            return View();
        }

        public ActionResult Centers()
        {
            return View();
        }

        public ActionResult CoreValues()
        {
            return View();
        }
        public ActionResult Glossary()
        {
            return View();
        }

        public ActionResult Hq()
        {
            return View();
        }

        public ActionResult IT()
        {
            return View();
        }

        public ActionResult Jbr()
        {
            return View();
        }
        public ActionResult Leaders()
        {
            return View();
        }
        public ActionResult Mod()
        {
            return View();
        }

        public ActionResult RsiCulture()
        {
            return View();
        }

        public ActionResult Source()
        {
            return View();
        }

        public ActionResult Training()
        {
            return View();
        }

        public ActionResult WhatIsRsi()
        {
            return View();
        }
    }
}