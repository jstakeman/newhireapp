$(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            init_date: {
                validators:{
                    stringLength:{
                        min: 10,
                    },
                    notEmpty:{
                        message: 'Please enter a valid date'
                    }
                }
            },
            hire_date: {
                validators:{
                    stringLength:{
                        min: 10,
                    },
                    notEmpty:{
                        message: 'Please enter a valid date'
                    }
                }
            },
			pref_first_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your preferred name'
                    }
                }
            },
			title: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your title'
                    }
                }
            },
            practice_area: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your practice area'
                    }
                }
            },
            date: {
                validators:{
                    stringLength: {
                        min: 10,
                    },
                        notEmpty:{
                        message: 'Please input a valid Date'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            personal_email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            location: {
                validators: {
                    notEmpty: {
                        message: 'Please select your Development Center'
                    }
                }
            },
        }
    })
    .on('success.form.bv', function(e) {
        alert("I got to the success function");
        $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...

        // Israel:
        // HTTP POST code 
        var dt = new Date();
        var utcDate = dt1.toUTCString();
        var req = {
        method: 'POST',
        url: 'localhost:3000/api/v1/newhires',
        headers: {
          'Content-Type': application/x-www-form-urlencoded
        },
        data: {
          first_name: document.getElementsByName("first_name")[0].value,
          last_name: document.getElementsByName("last_name")[0].value,
          preffered_name: document.getElementsByName("pref_first_name")[0].value,
          personal_email: document.getElementsByName("personal_email")[0].value,
          dev_center: document.getElementsByName("location")[0].value,
          title: document.getElementsByName("title")[0].value,
          employee_type: (document.getElementByClassName('btn-group')[0].elements)['options'].value,
          pratice_area: document.getElementsByName("practice_area")[0].value,
          date_initiated: document.getElementsByName("init_date")[0].value,
          date_of_hire: document.getElementsByName("hire_date")[0].value,
          created_at: utcDate,
          updated_at: utcDate
        }
      }
      $http(req).then(function successCallback(response){
        // This callback will be called asynchonously
        // when the response if available
        alert("Success!");
      }, function errorCallback(response){
        // Called asynchonously if an error occurs
        // or server returns response with an error status.
        alert("Failure!");
      });

            //
            // Original Code
            //
            $('#contact_form').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);
            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
    });
});