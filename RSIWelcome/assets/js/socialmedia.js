﻿// navigation2.js by Israel Montoya
// loads navigation into page from one level down.
document.write(`
    <div id="footer">
        <div class ="container">
            <div class ="row">
                <div class ="12u">
                    <!--Contact-->
                    <section class ="contact">
                        <header>
                            <h3>Want to hear from us on social media?</h3>
                        </header>
                        <p>Rural Sourcing Inc. is available on the following services</p>
                        <ul class ="icons">
                            <li><a href="https://twitter.com/RuralSourcing" class ="fa fa-twitter"><span class ="label"></span></a></li>
                            <li><a href="https://www.facebook.com/RuralSourcing/" class ="fa fa-facebook"><span class ="label"></span></a></li>
                            <li><a href="https://www.linkedin.com/company/rural-sourcing-inc-" class ="fa fa-linkedin"><span class ="label"></span></a></li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
`);