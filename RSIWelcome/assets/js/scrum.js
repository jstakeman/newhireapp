/*
    scrum.js - Handles Scrum Board for new hire page
    By: Israel Montoya - israel.montoya@ruralsourcing.com
*/

// JQuery to allow bootstrap tooltips
$(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    loadProgress();
});

// Index of the current site
var siteIndex = 0;
// The url of the site you want in the frame with default
var siteName = "todo/"+siteIndex+".html";
// Object string with modular URL
var fullString = '<object data="' + siteName + '" />';

function loadProgress() {
    var finishedPages = 0;
    // Loads stored progress from localStorage
    for (checkIndex = 0; checkIndex <= 13; checkIndex++) {
        var check = 'chk' + checkIndex;
        if (localStorage.getItem(check) == 'checked') {
            document.getElementById(check).checked = true;
        }
        else if (localStorage.getItem(check) == 'not checked') {
            document.getElementById(check).checked = false;
            if (finishedPages == 0) {
                finishedPages = checkIndex;
            }
        }
    }
    // Boolean that tracks if there is no progress on to-do checklist
    var allClear = true;
    // Checks to see if all navigation checkboxes are checked
    for (checkClear = 0; checkClear <= 13; checkClear++) {
        var check = 'chk' + checkClear;
        if (localStorage.getItem(check) == 'checked') {
            // If not, then sets allClear to false
            allClear = false;
        }
    }
    // If site has no progress, page 0 is loaded
    if (allClear) {finishedPages = 0;}
    siteIndex = finishedPages;
    selectPage();
    showProgress();
}
// This function checks the current index and chooses a site appropriately
function selectPage(){

    // Pages in the list
    if (siteIndex >= 0 && siteIndex <= 13) {siteName = "todo/"+siteIndex+".html";}
    // When there are no new sites to load, redirects to homepage
    else {
        // If all pages are complete
        if (document.getElementById('chk0').checked
        && document.getElementById('chk1').checked
        && document.getElementById('chk2').checked
        && document.getElementById('chk3').checked
        && document.getElementById('chk4').checked
        && document.getElementById('chk5').checked
        && document.getElementById('chk6').checked
        && document.getElementById('chk7').checked
        && document.getElementById('chk8').checked
        && document.getElementById('chk9').checked
        && document.getElementById('chk10').checked
        && document.getElementById('chk11').checked
        && document.getElementById('chk12').checked
        && document.getElementById('chk13').checked) {
            // Clears localstorage
            localStorage.clear();
            // Redirect to congradulations page
            window.location.href = "congratulations.html";
        }
        else {
            alert("You haven't completed all of the items on the to-do list!");
        }
    }
    // Sets the new url into the object string
    fullString = '<object data="'+siteName+'" />';

    // Reloads the site in frame
    $("#currentscrum").html(fullString);
}
function nextPage() {
    // Will not progress without Paperwork checkboxes
    if (siteIndex == 0) {
        if (localStorage.paper0 &&
            localStorage.paper1 &&
            localStorage.paper2 &&
            localStorage.paper3) { updatePage(); }
        else { alert("You haven't completed all the paperwork!") }
    }
    // Will not progress without Manager checkbox
    else if (siteIndex == 4) {
        if (localStorage.manager) { updatePage(); }
        else { alert("You haven't scheduled a meeting with your Manager!") }
    }
    // Will not progress without Mentor checkbox
    else if (siteIndex == 5) {
        if (localStorage.mentor) { updatePage(); }
        else { alert("You haven't scheduled a meeting with your Mentor!") }
    }
    // Will not progress without DCD checkbox
    else if (siteIndex == 6) {
        if (localStorage.dcd) { updatePage(); }
        else { alert("You haven't scheduled a meeting with your Development Center Director!") }
    }
    // Will not progress without Yammer checkboxes
    else if (siteIndex == 7) {
        if (localStorage.yammer0 &&
            localStorage.yammer1 &&
            localStorage.yammer2 &&
            localStorage.yammer3 &&
            localStorage.yammer4 &&
            localStorage.yammer5 &&
            localStorage.yammer6 &&
            localStorage.yammer7) { updatePage(); }
        else { alert("You haven't completely set up Yammer!") }
    }
    // Will not progress without Phone checkboxes on at least one of the systems
    else if (siteIndex == 8) {
        if (localStorage.phone1_0 &&
            localStorage.phone1_1 &&
            localStorage.phone1_2) { updatePage(); }
        else if (localStorage.phone2_0 &&
            localStorage.phone2_1) { updatePage(); }
        else { alert("You need to finish setting up your center's phone!")}
    }
    else{ updatePage(); }
    storeCheckbox();
}
function updatePage() {
    // Sets page checkbox to complete when next is pressed
    var checkString = "chk" + siteIndex;
    checkbox = document.getElementById(checkString);
    checkbox.checked = true;

    // Navigates to next page
    siteIndex++;
    selectPage();
    showProgress();
}
function storeCheckbox() {
    // Stores checked boxes in localstorage
    for (checkIndex = 0; checkIndex <= 13; checkIndex++) {
        var check = 'chk' + checkIndex;
        if (document.getElementById(check).checked) {
            localStorage.setItem(check, 'checked');
        }
        else {
            localStorage.setItem(check, 'not checked');
        }
    }
}
function goBack() {
      window.location.href = "index.html";
}
function prevPage(){
    siteIndex--;
    selectPage();
    showProgress();
}
function showProgress()
{
    // Shows current page in radio buttons
    var radioString = "rad"+siteIndex;
    radio = document.getElementById(radioString)
    radio.checked = true;
}
function skipPage() {
    // If user clicks on a pages navigation button, this function skips to that page.
    var selectedPage = document.getElementsByName("pageradio");

    for(var i = 0; i < selectedPage.length; i++) {
    if(selectedPage[i].checked == true) {
        siteIndex = i;
        }
    }
    selectPage();
}
$("#currentscrum").html(fullString);