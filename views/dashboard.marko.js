function create(__helpers) {
  var str = __helpers.s,
      empty = __helpers.e,
      notEmpty = __helpers.ne,
      escapeXml = __helpers.x,
      forEach = __helpers.f,
      attr = __helpers.a;

  return function render(data, out) {
    out.w("<!DOCTYPE html> <html> <head> <meta charset=\"UTF-8\"> <link rel=\"icon\" type=\"image/png\" href=\"favicon.png\"> <div id=\"redbar\"> </div> <title>Rural Sourcing Inc. | Dashboard</title> <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\"> <link rel=\"stylesheet\" href=\"style.css\"> </head> <body> <div class=\"container\"> <legend></legend> <legend> <a href=\"https://www.ruralsourcing.com/\"><img src=\"images/RSI_logo182x721.png\"></a> </legend> <a href=\"newhire\" class=\"btn btn-danger\">Add a New Colleague</a> <table class=\"table table-striped\" style=\"margin-top: 20px;\"> <thead> <tr> <th>First Name</th> <th>Last Name</th> <th>Email</th> <th>Status</th> <th></th> </tr> </thead> ");

    if (data.newhires) {
      out.w("<tbody> ");

      forEach(data.newhires, function(hire) {
        out.w("<tr> <td>" +
          escapeXml(hire.first_name) +
          "</td> <td>" +
          escapeXml(hire.last_name) +
          "</td> <td>" +
          escapeXml(hire.rsi_email) +
          "</td> ");

        if (hire.manager) {
          out.w("<td>DCD Complete</td>");
        }

        out.w(" ");

        if (!hire.manager) {
          out.w("<td>Waiting on DCD</td>");
        }

        out.w(" <td> <a class=\"btn\"" +
          attr("href", hire.hash) +
          ">Edit New Hire</a> </td> </tr>");
      });

      out.w(" </tbody>");
    }

    out.w(" </table> </div> <script src=\"http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script> </body> </html>");
  };
}

(module.exports = require("marko").c(__filename)).c(create);
