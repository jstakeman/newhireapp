var knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: "./db/devdb.sqlite"
  },
  useNullAsDefault: true
});

knex.schema.dropTableIfExists('newhires').then(function(){
  return knex.schema.createTableIfNotExists('newhires', function(table){
    table.string('first_name');
    table.string('last_name');
    table.string('hash').primary();
    table.string('preffered_name');
    table.string('email');
    table.string('rsi_email');
    table.string('personal_email');
    table.string('dev_center');
    table.string('title');
    table.string('employee_type');
    table.string('pratice_area');
    table.date('date_initiated');
    table.date('date_of_hire');
    table.string('manager');
    table.string('mentor');
    table.string('workspace');
    table.timestamps();
  })
}).then(function(){
return knex('newhires').insert([{
  'first_name': 'Edgar',
  'last_name': 'Vasquez',
  'preffered_name': 'Ed',
  'hash': '12345asdf'
},{
  'first_name': 'Israel',
  'last_name': 'Montoya',
  'hash': '5678asdf'
}]);
}).catch(function(e){
  console.log(e);
}).finally(function(){
  return knex.destroy();
})
